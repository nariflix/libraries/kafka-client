module gitlab.com/nariflix/libraries/kafka-client

go 1.22.0

require github.com/confluentinc/confluent-kafka-go v1.9.2 // indirect
