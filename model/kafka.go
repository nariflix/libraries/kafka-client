package model

import "strings"

type KafkaProperties struct {
	Brokers               []string
	Topic                 string
	GroupId               string
	AddressFamily         string
	Timeout               int
	AutoOffsetReset       string
	EnableAutoOffsetStore bool
}

func (kafkaProperties *KafkaProperties) New() *KafkaProperties {
	return &KafkaProperties{}
}

func (kafkaProperties *KafkaProperties) GetStringBrokers() string {

	if len(kafkaProperties.Brokers) == 0 {
		return ""
	}
	return strings.Join(kafkaProperties.Brokers, ",")
}
