package kafka_client

import (
	"gitlab.com/nariflix/libraries/kafka-client/consumer"
	"gitlab.com/nariflix/libraries/kafka-client/model"
)

var properties *model.KafkaProperties
var c *consumer.Consumer

func GetKafkaProperties() *model.KafkaProperties {
	if properties == nil {
		properties = &model.KafkaProperties{}
	}
	return properties
}

func GetKafkaConsumer(properties model.KafkaProperties) *consumer.Consumer {
	if c == nil {
		c = (&consumer.Consumer{}).New(properties)
	}
	return c
}
