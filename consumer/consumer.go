package consumer

import (
	"fmt"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"gitlab.com/nariflix/libraries/kafka-client/model"
	"os"
	"os/signal"
	"syscall"
)

type Consumer struct {
	Properties *model.KafkaProperties
}

func (consumer *Consumer) New(properties model.KafkaProperties) *Consumer {
	consumer.Properties = &properties
	return consumer
}

func configureNewConsumer(properties model.KafkaProperties) (*kafka.Consumer, error) {
	return kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": properties.GetStringBrokers(),
		// Avoid connecting to IPv6 brokers:
		// This is needed for the ErrAllBrokersDown show-case below
		// when using localhost brokers on OSX, since the OSX resolver
		// will return the IPv6 addresses first.
		// You typically don't need to specify this configuration property.
		"broker.address.family": properties.AddressFamily,
		"group.id":              properties.GroupId,
		"session.timeout.ms":    properties.Timeout,
		// Start reading from the first message of each assigned
		// partition if there are no previously committed offsets
		// for this group.
		"auto.offset.reset": properties.AutoOffsetReset,
		// Whether or not we store offsets automatically.
		"enable.auto.offset.store": properties.EnableAutoOffsetStore,
	})
}

func startConsuming(c *kafka.Consumer, receiver func(msg string)) {

	run := true

	fmt.Println("Starting Consumer")

	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	for run {
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			ev := c.Poll(10)
			if ev == nil {
				continue
			}
			run = handleMessage(c, ev, receiver)
		}
	}

	fmt.Printf("Closing consumer\n")
	err := c.Close()
	if err != nil {
		return
	}
}

func handleMessage(c *kafka.Consumer, ev kafka.Event, receiver func(msg string)) bool {
	fmt.Println("Ev: ", ev)
	switch e := ev.(type) {
	case *kafka.Message:

		// Process the message received.
		msgJson := string(e.Value)
		//fmt.Printf("%% Message on %s:\n%s\n", e.TopicPartition, msgJson)

		if e.Headers != nil {
			fmt.Printf("%% Headers: %v\n", e.Headers)
		}
		// We can store the offsets of the messages manually or let
		// the library do it automatically based on the setting
		// enable.auto.offset.store. Once an offset is stored, the
		// library takes care of periodically committing it to the broker
		// if enable.auto.commit isn't set to false (the default is true).
		// By storing the offsets manually after completely processing
		// each message, we can ensure atleast once processing.
		offsets, err := c.StoreMessage(e)
		if err != nil {
			fmt.Fprintf(os.Stderr, "%% Error storing offset after message %s:\n",
				e.TopicPartition, offsets)
		}
		receiver(msgJson)
	case kafka.Error:
		// Errors should generally be considered
		// informational, the client will try to
		// automatically recover.
		// But in this example we choose to terminate
		// the application if all brokers are down.
		_, err := fmt.Fprintf(os.Stderr, "%% Error: %v: %v\n", e.Code(), e)
		if err != nil {
			return true
		}
		if e.Code() == kafka.ErrAllBrokersDown {
			return false
		}
	default:
		fmt.Printf("Ignored %v\n", e)
	}
	return true
}

func (consumer *Consumer) Init(receiver func(msg string)) {

	c, err := configureNewConsumer(*consumer.Properties)

	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create consumer: %s\n", err)
		os.Exit(1)
	}

	fmt.Printf("Created Consumer %v\n", c)

	err = c.SubscribeTopics([]string{consumer.Properties.Topic}, nil)

	startConsuming(c, receiver)
}
